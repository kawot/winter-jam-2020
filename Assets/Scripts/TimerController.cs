﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerController : MonoBehaviour
{
    [SerializeField] string timeFormat = "{0:D1}:{1:D2}";
    [SerializeField] TextMeshProUGUI timerLabel;

    void Start()
    {
        
    }

    void Update()
    {
        var secondsLeft = Mathf.Max(0, GameController.clock);
        string timeText = string.Format(timeFormat, Mathf.FloorToInt(secondsLeft / 60f), Mathf.FloorToInt(secondsLeft % 60f));
        timerLabel.text = timeText;
    }
}
