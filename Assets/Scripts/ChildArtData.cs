﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "ChildArtData", menuName = "Config/Art", order = 1)]
public class ChildArtData : ScriptableObject
{
    public List<ChildBodyPart> parts;
}

[System.Serializable]
public class ChildBodyPart
{
    public string type;
    public Sprite[] variants;
}
