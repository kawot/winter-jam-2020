﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class Child : Client
{
    public static int karmaGenerated = 0;
    public static int goodGenerated = 0;

    [SerializeField] GameObject bubble;
    [SerializeField] GameObject giftAmination;
    [SerializeField] TextMeshProUGUI bubbleText;
    [SerializeField] TextMeshProUGUI replyText;
    [SerializeField] Image bigBubble;
    [SerializeField] Image smallBubble;
    [SerializeField] Image timer;
    public int Karma => speech.karma;

    private Speech speech;

    private new void Awake()
    {
        base.Awake();
        type = ClientType.Child;
        speech = AnswerManager.GetRandomAnswer;
    }

    private void Start()
    {
        replyText.text = "";
        bubbleText.text = "";
        bigBubble.color = Color.clear;
        smallBubble.color = Color.clear;
    }

    private void Update()
    {
        timer.transform.localScale = new Vector3(GameController.timer, 1f, 1f);
    }

    public override void Randomize()
    {
        // RANDOMIZE SPEECH (based on already generated karma?)
        var karmaNeeded = Random.Range(-1f, 1f) > karmaGenerated * 0.3f ? 1 : -1;
        var safeCounter = 0;
        while (Mathf.Sign(speech.karma) != karmaNeeded && speech.karma != 0)
        {
            speech = AnswerManager.GetRandomAnswer;

            safeCounter++;
            if (safeCounter > 1000)
            {
                Debug.LogWarning("Speech with karma needed not found");
                // IF USING FINITE LIST RESTOCK LINES HERE
                break;
            }
        }

        if (speech.line == "") speech.line = "no text in line";
        if (speech.giftReply == "") speech.giftReply = "thanks!";
        if (speech.rejectReply == "") speech.rejectReply = "why?!!!";
        karmaGenerated += speech.karma;
        if (speech.karma > 0) goodGenerated++;
        timer.color = Color.clear;
    }

    public void ShowBubble(bool show, LineType type)
    {
        switch (type)
        {
            case LineType.Speech:
                replyText.text = "";
                bubbleText.text = speech.line;
                timer.color = new Color(0.2f, 0.6f, 0f, 1f);
                bigBubble.color = Color.white;
                smallBubble.color = Color.clear;
                break;
            case LineType.Gift:
                replyText.text = speech.giftReply;
                bubbleText.text = "";
                timer.color = Color.clear;
                bigBubble.color = Color.clear;
                smallBubble.color = Color.white;
                break;
            case LineType.Reject:
                replyText.text = speech.rejectReply;
                bubbleText.text = "";
                timer.color = Color.clear;
                bigBubble.color = Color.clear;
                smallBubble.color = Color.white;
                break;
            default:
                break;
        }
        transform.DOScaleZ(1f, 3.5f).OnComplete(() => smallBubble.gameObject.SetActive(false));
    }

    public void GiftAnimation(bool gift, Action callback)
    {
        //ANIMATION WITH CALLBACK ON FINISH
        if (gift)
        {
            giftAmination.SetActive(true);
            transform.DOScaleZ(1f, 0.5f).OnComplete(() => callback());
        }
        else
        {
            callback();
        }
    }
}

public enum LineType
{
    Speech,
    Gift,
    Reject
}
