﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChildGenerator : MonoBehaviour
{
    [SerializeField] ChildArtData data;

    public void Generate()
    {
        var parts = GetComponentsInChildren<Image>();
        for (var i = 0; i < parts.Length; i++)
        {
            parts[i].name = data.parts[i].type;
            parts[i].sprite = data.parts[i].variants[Random.Range(0, data.parts[i].variants.Length)];
        }
    }
}
