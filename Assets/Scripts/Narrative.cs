﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NarrativeData", menuName = "Config/Lines", order = 1)]
public class Narrative : ScriptableObject
{
    public List<Speech> lines;
}

[System.Serializable]
public class Speech
{
    public int karma;
    public string line;
    public string giftReply;
    public string rejectReply;

    public Speech()
    {
        
    }

    public Speech(Speech clone)
    {
        karma = clone.karma;
        line = clone.line;
        giftReply = clone.giftReply;
        rejectReply = clone.rejectReply;
    }
}
