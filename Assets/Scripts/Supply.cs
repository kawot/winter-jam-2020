﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class Supply : Client
{
    public int amount = 5;
    [SerializeField] float flyTime = 0.5f;

    private new void Awake()
    {
        base.Awake();
        type = ClientType.Supply;
    }

    public void PlayAnimation(Action callback)
    {
        animator.SetBool("fly only", true);
        animator.SetTrigger("fly");
        transform.DOScaleZ(1f, flyTime).OnComplete(() => callback());
    }
}