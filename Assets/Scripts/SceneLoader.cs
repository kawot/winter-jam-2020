﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private string _sceneName;

    void Awake()
    {
        _button.onClick.AddListener(() => SceneManager.LoadScene(_sceneName));
    }
}
