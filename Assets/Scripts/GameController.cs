﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public static int giftsLeft;
    public static int karma;
    public static int giftsGenerated = 0;
    public static float timer = 0;
    public static float clock;

    [SerializeField] float stepTime = 0.66f;
    [SerializeField] float chainTime = 0.5f;
    [SerializeField] float waitTime = 2f;
    [SerializeField] float replyTime = 1f;
    [SerializeField] float startClock = 90f;
    [SerializeField] int lineLength = 5;
    [SerializeField] int giftsStartAmount = 5;
    [SerializeField] Client[] children;
    [SerializeField] Client supply;

    private List<Client> line = new List<Client>();
    private Client leaver;
    private Client currentClient;
    private bool isActive = false;
    private int lastRND =-1;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        karma = 0;
        clock = startClock;
        giftsLeft = giftsStartAmount;

        for (var i = 0; i < lineLength; i++)
        {
            line.Add(NewClient());
        }

        for (var i = 0; i < line.Count; i++)
        {
            line[i].MoveToSpot(i, 0f);
        }

        transform.DOMoveX(0f, 1f).OnComplete(HandleClient);
    }

    private void Update()
    {
        clock -= Time.deltaTime;

        if (clock < 0)
            SceneManager.LoadScene("Finish");

        if (!isActive) return;

        timer -= Time.deltaTime / waitTime;
        if (timer < 0) StartTimoOut();
    }

    private Client NewClient()
    {
        Client client;
        if (Child.goodGenerated > giftsGenerated + 1)
        {
            client = Instantiate(supply, UIController.instance.spawnSpot.position, Quaternion.identity, UIController.instance.spawnSpot.transform.parent);
            giftsGenerated += ((Supply)client).amount;
        }
        else
        {
            var rnd = Random.Range(0, children.Length);
            while (rnd == lastRND && children.Length > 1)
            {
                rnd = Random.Range(0, children.Length);
            }
            lastRND = rnd;
            client = Instantiate(children[rnd], UIController.instance.spawnSpot.position, Quaternion.identity, UIController.instance.spawnSpot.transform.parent);
        }
        client.Randomize();
        client.transform.SetSiblingIndex(0);
        return client;
    }

    private void Next()
    {
        line.Add(NewClient());
        if (leaver != null)
        {
            leaver.MoveToSpot(-2, stepTime);
            Destroy(leaver.gameObject, stepTime + 1f);
        }
        leaver = line[0];
        leaver.MoveToSpot(-1, stepTime, replyTime);
        leaver.transform.SetSiblingIndex(0);
        line.RemoveAt(0);
        for (var i = 0; i < line.Count; i++)
        {
            line[i].MoveToSpot(i, stepTime, chainTime * (i + 1) + replyTime);
        }
        transform.DOMoveX(0f, stepTime + replyTime + chainTime).OnComplete(HandleClient);
    }

    private void HandleClient()
    {
        isActive = true;
        timer = 1f;

        currentClient = line[0];
        switch (currentClient.type)
        {
            case ClientType.Child:
                var child = (Child)currentClient;
                child.ShowBubble(true, LineType.Speech);
                UIController.instance.ShowButtons(true);
                break;
            case ClientType.Supply:
                var supply = (Supply)currentClient;
                UIController.instance.ShowSupplyButton(true);
                break;
            default:
                break;
        }
    }

    public void StartSupply()
    {
        isActive = false;
        var supply = (Supply)currentClient;
        supply.PlayAnimation(Resupply);
        UIController.instance.HideButtons();
    }

    private void Resupply()
    {
        var supply = (Supply)currentClient;
        giftsLeft += supply.amount;
        UIController.instance.SupplyAnimation();
        Next();
    }

    public void StartGift()
    {
        isActive = false;
        giftsLeft--;
        var child = (Child)currentClient;
        child.GiftAnimation(true, EndGift);
        UIController.instance.HideButtons();
    }

    private void EndGift()
    {
        var child = (Child)currentClient;
        child.ShowBubble(true, LineType.Gift);
        karma += child.Karma;
        Next();
    }

    public void StartTimoOut()
    {
        isActive = false;
        if (currentClient.type == ClientType.Child)
        {
            var child = (Child)currentClient;
            child.GiftAnimation(false, EndTimeOut);
        }
        else
        {
            StartSupply();
        }
        UIController.instance.HideButtons();
    }

    private void EndTimeOut()
    {
        var child = (Child)currentClient;
        child.ShowBubble(true, LineType.Reject);
        karma -= child.Karma;
        Next();
    }
}
