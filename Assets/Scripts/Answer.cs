﻿public struct Answer
{
	public string Replica;
	public string GiftReceived;
	public string GiftNotReceived;
	public int Karma;
}
