﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using TMPro;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    public List<RectTransform> lineSpots = new List<RectTransform>();
    public RectTransform spawnSpot;
    [SerializeField] TextMeshProUGUI giftsLeftLabel;
    [SerializeField] TextMeshProUGUI karmaLabel;
    [SerializeField] GameObject supplyButton;
    [SerializeField] GameObject[] gifts;
    [SerializeField] GameObject supplyAnimation;
    [SerializeField] GameObject giveOutAnimation;

    private bool choiceAvailable = false;
    private bool supplyAvailable = false;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        RedrawGifts();
    }

    private void Update()
    {
        karmaLabel.text = "KARMA " + GameController.karma.ToString();

        if (Input.GetMouseButton(0) && supplyAvailable) OnSupply();
    }

    public void ShowButtons(bool show)
    {
        // ENABLE/DISABLE CHOICE BUTTONS
        choiceAvailable = show;
    }

    public void ShowSupplyButton(bool show)
    {
        // ENABLE/DISABLE SUPPLY
        // supplyButton.SetActive(show);
        supplyAvailable = show;
    }

    public void HideButtons()
    {
        ShowButtons(false);
        ShowSupplyButton(false);
    }

    public void OnGift()
    {
        if (GameController.giftsLeft > 0 && choiceAvailable)
        {
            RedrawGifts(-1);
            giveOutAnimation.SetActive(true);
            supplyAnimation.transform.DOScaleZ(1f, 0.15f).OnComplete(() =>
            {
                GameController.instance.StartGift();
                giveOutAnimation.SetActive(false);
            });
        }       
    }

    //public void OnReject()
    //{
    //    if (choiceAvailable) GameController.instance.StartReject();
    //}

    public void OnSupply()
    {
        if (supplyAvailable) GameController.instance.StartSupply();
    }

    public void SupplyAnimation()
    {
        supplyAnimation.SetActive(true);
        supplyAnimation.transform.DOScaleZ(1f, 1f).OnComplete(() =>
        {
            RedrawGifts();
            supplyAnimation.SetActive(false);
        });       
    }

    private void RedrawGifts(int bias = 0)
    {
        for (int i = 0; i < gifts.Length; i++)
        {
            gifts[i].SetActive(GameController.giftsLeft + bias > i);
        }
        giftsLeftLabel.text = (GameController.giftsLeft + bias).ToString();
    }
}