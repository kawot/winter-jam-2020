﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AnswerManager : MonoBehaviour
{
	private string _resourceName = @"Applicant Lines - Sheet1";

	private static AnswerManager _manager;
	List<Speech> _answers = new List<Speech>();

	void Awake()
	{
		if (_manager != null)
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);
		_manager = this;

		using (var reader = new StringReader(Resources.Load<TextAsset>(_resourceName).text))
		{
			var line = reader.ReadLine();
			line = reader.ReadLine();

			do
			{
				var subStrs = line.Split(',');

				for (int i = 0; i < subStrs.Length; i++)
					subStrs[i] = subStrs[i].Replace(';', ',');

				_answers.Add(new Speech()
				{
					giftReply = subStrs[0],
					rejectReply = subStrs[1],
					karma = int.Parse(subStrs[2]),
					line = subStrs[3]
				});

				line = reader.ReadLine();
			} while (!string.IsNullOrEmpty(line));
		}
	}

	#region API

	public static List<Speech> GetAnswers => _manager._answers;

	public static Speech GetRandomAnswer => _manager._answers[Random.Range(0, _manager._answers.Count)];

	#endregion
}