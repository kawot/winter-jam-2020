﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Conveyor : MonoBehaviour
{
    [SerializeField] float stepDistance = 200f;
    [SerializeField] float repeatDistance = 500f;
    [Space(10)]
    [SerializeField] RectTransform conveyor;
    [SerializeField] RectTransform belt;

    private void Awake()
    {
    }

    public void Move(float time)
    {
        belt.DOMoveX(-stepDistance, time).SetRelative(true).SetEase(Ease.InOutSine).OnComplete(Restore);
    }

    private void Restore()
    {
        var pos = belt.anchoredPosition;
        pos.x = -Mathf.RoundToInt((conveyor.anchoredPosition.x / repeatDistance));
        belt.anchoredPosition = pos;
    }
}
