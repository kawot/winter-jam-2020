﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class Client : MonoBehaviour
{
    public ClientType type;
    [SerializeField] protected Animator animator;

    private RectTransform rt;

    protected void Awake()
    {
        rt = GetComponent<RectTransform>();
    }

    public void MoveToSpot(int i, float time, float delay = 0f)
    {
        //TWEEN MOVEMENT TO UIController.instance.lineSpots[i+2];
        rt.DOAnchorPos(rt.anchoredPosition, delay).OnComplete(() =>
        {
            rt.DOAnchorPos(UIController.instance.lineSpots[i + 2].anchoredPosition, time).SetEase(Ease.InOutSine);
            if (time != 0f && animator != null) animator.SetTrigger("walk");
        });
    }

    public virtual void Randomize()
    {

    }
}

public enum ClientType
{
    Child,
    Supply
}
