﻿Shader "Hidden/light"
{
    Properties
    {
		_Cone ("Cone Factor", Range(0,1)) = 0.5
		_Gradient ("Cone Factor", Range(0,1)) = 0
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
			};

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				o.color = v.color;
                return o;
            }

            sampler2D _MainTex;
			fixed _Cone;
			fixed _Gradient;

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = i.color;
				col.a *= lerp(1.0, i.uv.y, _Gradient) * smoothstep (abs(i.uv.x - 0.5), abs(i.uv.x - 0.5) + 0.01,(1.0 - i.uv.y * _Cone) * 0.5);
                //return abs(i.uv.x - 0.5);
                return col;
            }
            ENDCG
        }
    }
}
